# Chromatographic data segmentation tree method - SCRIPT1
# Copyright 2018 Tomas Drevinskas
# Apache License, Version 2.0

duomenys <- AllData
duomenys.features <- duomenys
duomenys.processed <- NULL

levelNo <- 7 # the number of levels for the segmentation tree

refMean <- mean(duomenys.features$RT)
refMinimum <- min(duomenys.features$RT)
refMaximum <- max(duomenys.features$RT)
n <- length(duomenys.features$RT)
segmentData <- NULL

refRTcol <- 3
refRTcolRead <- 2
numberOfMeans <- NULL
RTerror <- 0.0025 #retention time tolerance factor RTtf (for GC-MS 0.0025 min)

dfData <- data.frame(num = 1:n)
dfSegmentMeans <- data.frame()
dfSegmentMinimum <- data.frame()
dfSegmentMaximum <- data.frame()
columnIndex <- 1
dfData[columnIndex] <- duomenys.features$RT
columnIndex <- columnIndex + 1
dfData[columnIndex] <- duomenys.features$AREA
columnIndex <- columnIndex + 1

dfSegmentMeans[1, 1] <- refMean
dfSegmentMeans[2, 2] <- refMean
dfSegmentMeans[1, 2] <- refMean

dfSegmentMinimum[1, 1] <- refMinimum
dfSegmentMinimum[2, 2] <- refMinimum
dfSegmentMinimum[1, 2] <- refMinimum

dfSegmentMaximum[1, 1] <- refMaximum
dfSegmentMaximum[2, 2] <- refMaximum
dfSegmentMaximum[1, 2] <- refMaximum

for (l in 1:levelNo) {
  if (l == 1) {
    numberOfMeans <- 1
  }
  else
    numberOfMeans <- 2 ^ (l - 1)
  for (m in 1:numberOfMeans) {
    segmentNumber <- m * 2
      for (s in 1:segmentNumber) {
        segmentData <- NULL
        if (s%%2 == 1) {
          # segmentMean <- refMean
          # segmentMinimum <- refMinimum
          # segmentMaximum <- refMaximum
        segmentMean <- dfSegmentMeans[s/2+0.5, refRTcol - 1]
        segmentMinimum <- dfSegmentMinimum[s/2+0.5, refRTcol - 1]
        segmentMaximum <- dfSegmentMaximum[s/2+0.5, refRTcol - 1]
        }
        else if (s%%2 == 0) {
          mentMean <- dfSegmentMeans[s/2, refRTcol - 1]
          segmentMinimum <- dfSegmentMinimum[s/2, refRTcol - 1]
          segmentMaximum <- dfSegmentMaximum[s/2, refRTcol - 1]
        }
        for (k in 1:n) {
          if (dfData[k, 1] <= segmentMean &
              dfData[k, 1] >= segmentMinimum & s %% 2 == 1) {
            segmentData[k] <- dfData[k, 1]
          }
          
          else if (dfData[k, 1] > segmentMean &
                   dfData[k, 1] <= segmentMaximum & s %% 2 == 0) {
            segmentData[k] <- dfData[k, 1]
          }
          else {
            # dfData[k, columnIndex] <- 'NA'
          }
        }
        dfSegmentMeans[s, refRTcol] <- mean(segmentData, na.rm = TRUE)
        dfSegmentMinimum[s, refRTcol] <- min(segmentData, na.rm = TRUE)
        dfSegmentMaximum[s, refRTcol] <- max(segmentData, na.rm = TRUE)
      }
  }
  
  # for (m in 1:numberOfMeans) {
  #   for (sd in 1:segmentNumber) {
  #     columnIndex1st <- columnIndex
  #     columnIndex2nd <- columnIndex + 1
  #     if (l == 1 & m == 1) {
  #       segmentMean <- refMean
  #       segmentMinimum <- refMinimum
  #       segmentMaximum <- refMaximum
  #     }
  #     else {
  #       refRTcolRead <- refRTcol - 1
  #       segmentMean <- dfSegmentMeans[m, refRTcolRead]
  #       segmentMinimum <- dfSegmentMinimum[m, refRTcolRead]
  #       segmentMaximum <- dfSegmentMaximum[m, refRTcolRead]
  #     }
  #     # statistical values: avg, min, max
  #     dfData[n + 1, columnIndex] <- segmentMinimum
  #     dfData[n + 2, columnIndex] <- segmentMean
  #     dfData[n + 3, columnIndex] <- segmentMaximum
  #     for (k in 1:n) {
  #       if (dfData[k, 1] <= segmentMean & dfData[k, 1] >= segmentMinimum) {
  #         # if (dfData[k, 1] <= segmentMean & dfData[k, 1] >= segmentMinimum & s == 1) {
  #         dfData[k, columnIndex1st] <- dfData[k, 2]
  #         # segmentData[k] <- dfData[k, 1]
  #       }
  #       else {
  #         dfData[k, columnIndex1st] <- 'NA'
  #       }
  #       if (dfData[k, 1] > segmentMean &
  #           dfData[k, 1] <= segmentMaximum) {
  #         # else if (dfData[k, 1] > segmentMean & dfData[k, 1] <= segmentMaximum & s == 2) {
  #         dfData[k, columnIndex2nd] <- dfData[k, 2]
  #         # segmentData[k] <- dfData[k, 1]
  #       }
  #       else {
  #         dfData[k, columnIndex2nd] <- 'NA'
  #       }
  #     }
  #     # print(columnIndex)
  #     # print(refRTcol)
  #     # print(l)
  #     columnIndex <- columnIndex + 2
  #   }
  # }
  
  refRTcol <- refRTcol + 1
  print(l)
}
# print(dfData)
# print(dfSegmentMinimum)
# print(dfSegmentMeans)
# print(dfSegmentMaximum)
write.table(dfSegmentMeans, "Means_ALL_levels.txt", sep="\t")
write.table(dfSegmentMinimum, "Minimum_ALL_levels.txt", sep="\t")
write.table(dfSegmentMaximum, "Maximum_ALL_levels.txt", sep="\t")