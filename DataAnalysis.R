# Chromatographic data segmentation tree method - SCRIPT1
# Copyright 2018 Tomas Drevinskas
# Apache License, Version 2.0

library(readxl)

fileNo <- 16 #number of files to process
valueNo <- 20 # number of lowest correlations to inspect
valueNoH <- 20 # number of highest correlations to inspect

dfDataTable <- NULL
dfDataTable <- data.frame()
dfDataTable <- read.delim("correlData.txt")
dfRTDataTable <- NULL
dfRTDataTable <- read.delim("retentionData.txt")
# dfDataTable <- read.delim("correlData")

# substances <- read_excel("substances.xlsx")

dfAnalyzedData <- NULL
dfAnalyzedData <-data.frame()
dfAnalyzedDataH <- NULL
dfAnalyzedDataH <-data.frame()

dfFullAnalyzedData <- NULL
dfFullAnalyzedData <-data.frame()
dfFullAnalyzedDataH <- NULL
dfFullAnalyzedDataH <-data.frame()

PlantList <- read_excel("list.xlsx")

dfAnalyzedData[2,1] <- "line"
dfAnalyzedData[3,1] <- "Correlation"
dfAnalyzedData[4,1] <- "SegmentMIN"
dfAnalyzedData[5,1] <- "SegmentMEAN"
dfAnalyzedData[6,1] <- "SegmentMax"
dfAnalyzedData[7,1] <- "1-lower_0-higher"

dfAnalyzedDataH[2,1] <- "line"
dfAnalyzedDataH[3,1] <- "Correlation"
dfAnalyzedDataH[4,1] <- "SegmentMIN"
dfAnalyzedDataH[5,1] <- "SegmentMEAN"
dfAnalyzedDataH[6,1] <- "SegmentMax"
dfAnalyzedDataH[7,1] <- "1-lower_0-higher"

dfFullAnalyzedData[2,1] <- "line"
dfFullAnalyzedData[3,1] <- "Correlation"
dfFullAnalyzedData[4,1] <- "SegmentMIN"
dfFullAnalyzedData[5,1] <- "SegmentMEAN"
dfFullAnalyzedData[6,1] <- "SegmentMax"
dfFullAnalyzedData[7,1] <- "1-lower_0-higher"

dfFullAnalyzedDataH[2,1] <- "line"
dfFullAnalyzedDataH[3,1] <- "Correlation"
dfFullAnalyzedDataH[4,1] <- "SegmentMIN"
dfFullAnalyzedDataH[5,1] <- "SegmentMEAN"
dfFullAnalyzedDataH[6,1] <- "SegmentMax"
dfFullAnalyzedDataH[7,1] <- "1-lower_0-higher"

correlationArray <- NULL
correlationArray <- read_excel("refCorVals.xlsx")

highestValues <- head(sort(dfDataTable$X1.length.datax1.V1., decreasing = TRUE), valueNoH)
lowestValues <- head(sort(dfDataTable$X1.length.datax1.V1.), valueNo)

for (i in 1:fileNo){
  dfAnalyzedData[i+7, 1] <- PlantList$NAME[i]
  dfAnalyzedData[i+7, 2] <- correlationArray$REFERENCE[i]
  dfAnalyzedDataH[i+7, 1] <- PlantList$NAME[i]
  dfAnalyzedDataH[i+7, 2] <- correlationArray$REFERENCE[i]
  dfFullAnalyzedData[i+7, 1] <- PlantList$NAME[i]
  dfFullAnalyzedData[i+7, 2] <- correlationArray$REFERENCE[i]
  dfFullAnalyzedDataH[i+7, 1] <- PlantList$NAME[i]
  dfFullAnalyzedDataH[i+7, 2] <- correlationArray$REFERENCE[i]
  
  dfFullAnalyzedData[i+7+fileNo+1, 1] <- PlantList$NAME[i]
  dfFullAnalyzedData[i+7+fileNo+1, 2] <- correlationArray$REFERENCE[i]
  dfFullAnalyzedDataH[i+7+fileNo+1, 1] <- PlantList$NAME[i]
  dfFullAnalyzedDataH[i+7+fileNo+1, 2] <- correlationArray$REFERENCE[i]
}

# whichIndex <- 3
# for (i in 1:valueNo){
#   # peakSum <- NULL
#   whichList <- which(grepl(lowestValues[i], dfDataTable$X1.length.datax1.V1.))
#   for (l in 1:length(whichList)){
#     substanceIndex <- which(grepl(whichList[l], substances$LINE))
#     dfFullAnalyzedData[2, whichIndex] <- whichList[l]
#     dfFullAnalyzedData[3, whichIndex] <- lowestValues[i]
#     dfFullAnalyzedData[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
#     # print(dfDataTable$V2[whichList[l]])
#     # print(dfFullAnalyzedData[4, whichIndex])
#     dfFullAnalyzedData[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[whichList[l]]))
#     dfFullAnalyzedData[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
#     dfFullAnalyzedData[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
#     substanceLocalList <- substances[substanceIndex,]
#     for (k in 1:fileNo){
#       dfFullAnalyzedData[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
#       dfFullAnalyzedData[7+k, whichIndex+1] <- dfRTDataTable[whichList[l],5+k]
#       # peakSum <- peakSum + dfRTDataTable[whichList[l],5+k]
#     }
#     for (s in 1:length(substanceLocalList)){
#       if (length(substanceIndex) > 0){
#         dfFullAnalyzedData[7+fileNo+s, whichIndex] <- substances[substanceIndex, s]
#       }
#     }
#     dfFullAnalyzedData[7+fileNo+1, whichIndex+1] <- colSums(dfFullAnalyzedData[whichIndex+1], na.rm=TRUE, dims=1)
#     dfFullAnalyzedData[7+fileNo+1, whichIndex] <- ""
#     whichIndex <- whichIndex + 2
#   }
# }
# 
# whichIndex <- 3
# for (i in 1:valueNo){
#   whichList <- which(grepl(lowestValues[i], dfDataTable$X1.length.datax1.V1.))
#   for (l in 1:length(whichList)){
#     substanceIndex <- which(grepl(whichList[l], substances$LINE))
#     dfAnalyzedData[2, whichIndex] <- whichList[l]
#     dfAnalyzedData[3, whichIndex] <- lowestValues[i]
#     dfAnalyzedData[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
#     dfAnalyzedData[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[[whichList[l]]]))
#     dfAnalyzedData[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
#     dfAnalyzedData[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
#     substanceLocalList <- substances[substanceIndex,]
#     for (k in 1:fileNo){
#       dfAnalyzedData[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
#     }
#     for (s in 1:length(substanceLocalList)){
#       if (length(substanceIndex) > 0){
#         dfAnalyzedData[7+fileNo+s, whichIndex] <- substances[substanceIndex, s]
#       }
#     }
#     # dfAnalyzedData[7+fileNo+1, whichIndex] <- colSums(dfFullAnalyzedData[whichIndex+1], na.rm=TRUE, dims=1)
#     dfAnalyzedData[7+fileNo+1, whichIndex] <- ""
#     whichIndex <- whichIndex + 1
#   }
# }

whichIndex <- 3
for (i in 1:valueNo){
  whichList <- which(grepl(lowestValues[i], dfDataTable$X1.length.datax1.V1.))
  for (l in 1:length(whichList)){
    dfAnalyzedData[2, whichIndex] <- whichList[l]
    dfAnalyzedData[3, whichIndex] <- lowestValues[l]
    dfAnalyzedData[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
    dfAnalyzedData[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[whichList[l]]))
    dfAnalyzedData[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
    dfAnalyzedData[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
    for (k in 1:fileNo){
      dfAnalyzedData[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
    }
    whichIndex <- whichIndex + 1
  }
}
whichIndex <- 3
for (i in 1:valueNo){
  whichList <- which(grepl(lowestValues[i], dfDataTable$X1.length.datax1.V1.))
  for (l in 1:length(whichList)){
    dfFullAnalyzedData[2, whichIndex] <- whichList[l]
    dfFullAnalyzedData[3, whichIndex] <- lowestValues[l]
    dfFullAnalyzedData[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
    dfFullAnalyzedData[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[whichList[l]]))
    dfFullAnalyzedData[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
    dfFullAnalyzedData[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
    for (k in 1:fileNo){
      currentSampleData <- NULL
      currentSampleDataStr <- paste(toString(k), sxlsx, sep = "")
      currentSampleData <- read_excel(currentSampleDataStr)
      
      dfFullAnalyzedData[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
      dfFullAnalyzedData[7+k, whichIndex+1] <- dfRTDataTable[whichList[l],5+k]
      if (as.numeric(as.character(dfRTDataTable[whichList[l],5+k])) != 0){
      for (c in 1 : as.numeric(as.character(dfRTDataTable[whichList[l],5+k]))){
        if (length(currentSampleData$RT) != 0){
        for (p in 1 : length(currentSampleData$RT)){
        # if (as.numeric(as.character(dfDataTable$V5[whichList[l]])) == 1){
          if (as.numeric(as.character(dfDataTable$V2[whichList[l]])) <= as.numeric(as.character(currentSampleData$RT[p])) && as.numeric(as.character(dfDataTable$V4[whichList[l]])) >= as.numeric(as.character(currentSampleData$RT[p]))){
          dfFullAnalyzedData[7+k+fileNo+c, whichIndex] <- currentSampleData$SUBSTANCE[p]
          }
        # }
        # else if (as.numeric(as.character(dfDataTable$V5[whichList[l]])) == 0){
          # if (as.numeric(as.character(dfDataTable$V3[whichList[l]])) <= as.numeric(as.character(currentSampleData$RT[p])) && as.numeric(as.character(dfDataTable$V4[whichList[l]])) >= as.numeric(as.character(currentSampleData$RT[p]))){
            # dfFullAnalyzedData[7+k+fileNo+1, whichIndex] <- currentSampleData$SUBSTANCE[p]
          # }
        # }
        }
        }
      }
      }
    }
    dfFullAnalyzedData[7+fileNo+1, whichIndex+1] <- colSums(dfFullAnalyzedData[whichIndex+1], na.rm=TRUE, dims=1)
    dfFullAnalyzedData[7+fileNo+1, whichIndex] <- ""
    whichIndex <- whichIndex + 2
  }
}

whichIndex <- 3
for (i in 1:valueNoH){
  whichList <- which(grepl(highestValues[i], dfDataTable$X1.length.datax1.V1.))
  for (l in 1:length(whichList)){
    dfAnalyzedDataH[2, whichIndex] <- whichList[l]
    dfAnalyzedDataH[3, whichIndex] <- highestValues[i]
    dfAnalyzedDataH[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
    dfAnalyzedDataH[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[whichList[l]]))
    dfAnalyzedDataH[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
    dfAnalyzedDataH[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
    for (k in 1:fileNo){
      dfAnalyzedDataH[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
    }
    whichIndex <- whichIndex + 1
  }
}
whichIndex <- 3
for (i in 1:valueNoH){
  whichList <- which(grepl(highestValues[i], dfDataTable$X1.length.datax1.V1.))
  for (l in 1:length(whichList)){
    dfFullAnalyzedDataH[2, whichIndex] <- whichList[l]
    dfFullAnalyzedDataH[3, whichIndex] <- highestValues[i]
    dfFullAnalyzedDataH[4, whichIndex] <- as.numeric(as.character(dfDataTable$V2[whichList[l]]))
    dfFullAnalyzedDataH[5, whichIndex] <- as.numeric(as.character(dfDataTable$V3[whichList[l]]))
    dfFullAnalyzedDataH[6, whichIndex] <- as.numeric(as.character(dfDataTable$V4[whichList[l]]))
    dfFullAnalyzedDataH[7, whichIndex] <- as.numeric(as.character(dfDataTable$V5[whichList[l]]))
    for (k in 1:fileNo){
      currentSampleData <- NULL
      currentSampleDataStr <- paste(toString(k), sxlsx, sep = "")
      currentSampleData <- read_excel(currentSampleDataStr)
      
      dfFullAnalyzedDataH[7+k, whichIndex] <- dfDataTable[whichList[l],5+k]
      dfFullAnalyzedDataH[7+k, whichIndex+1] <- dfRTDataTable[whichList[l],5+k]
      if (as.numeric(as.character(dfRTDataTable[whichList[l],5+k])) != 0){
      for (c in 1 : as.numeric(as.character(dfRTDataTable[whichList[l],5+k]))){
        if (length(currentSampleData$RT) != 0){
        for (p in 1 : length(currentSampleData$RT)){
          # if (as.numeric(as.character(dfDataTable$V5[whichList[l]])) == 1){
            if (as.numeric(as.character(dfDataTable$V2[whichList[l]])) <= as.numeric(as.character(currentSampleData$RT[p])) && as.numeric(as.character(dfDataTable$V4[whichList[l]])) >= as.numeric(as.character(currentSampleData$RT[p]))){
              dfFullAnalyzedDataH[7+k+fileNo+1, whichIndex] <- currentSampleData$SUBSTANCE[p]
            # }
          }
          # else if (as.numeric(as.character(dfDataTable$V5[whichList[l]])) == 0){
            # if (as.numeric(as.character(dfDataTable$V3[whichList[l]])) <= as.numeric(as.character(currentSampleData$RT[p])) && as.numeric(as.character(dfDataTable$V4[whichList[l]])) >= as.numeric(as.character(currentSampleData$RT[p]))){
              # dfFullAnalyzedDataH[7+k+fileNo+c, whichIndex] <- currentSampleData$SUBSTANCE[p]
            # }
          }
        }
        }
      }
      }
    }
    dfFullAnalyzedDataH[7+fileNo+1, whichIndex+1] <- colSums(dfFullAnalyzedDataH[whichIndex+1], na.rm=TRUE, dims=1)
    dfFullAnalyzedDataH[7+fileNo+1, whichIndex] <- ""
    whichIndex <- whichIndex + 2
  }

write.table(dfAnalyzedData, "AnalyzedDataL.txt", sep="\t")
write.table(dfAnalyzedDataH, "AnalyzedDataH.txt", sep="\t")
write.table(dfFullAnalyzedData, "FullAnalyzedDataL.txt", sep="\t")
write.table(dfFullAnalyzedDataH, "FullAnalyzedDataH.txt", sep="\t")
print(lowestValues)
print(highestValues)
