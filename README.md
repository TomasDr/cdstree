# README #

Chromatographic data segmentation tree method. To run it, read and follow �getting started CDStree.pdf�. The method is under review in journal � Nature Methods.

### What is this repository for? ###

* Chromatographic data segmentation tree method. This is chromatographic data ordering technique
* Version 0.1

### How do I get set up? ###

* Rstudio is needed to run these scripts
* �readxl� and �xlsx� libraries are required
* The code was written for scientific purposes, therefore it does contain �dirty-coding� parts and currently not all comments are provided
* To run it, read and follow �getting started CDStree.pdf�.

### Contribution guidelines ###

* None at the moment

### Who do I talk to? ###

* Repo owner or admin

